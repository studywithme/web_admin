import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [
  { state: 'users', name: 'Users', type: 'link', icon: 'portrait' },
  { state: 'rooms', type: 'link', name: 'Room', icon: 'meeting_room' },
  { state: 'channels', type: 'link', name: 'Chat Channel', icon: 'forum' },
  { state: 'logout', type: 'link', name: 'Logout', icon: 'logout' },
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
