import { User } from "../models/user.model";

export const ELEMENT_DATA: User[] = [
  {id: 1, name: 'ducmh01', email: 'ducmh01@gmail.com', avatar: 'https://picsum.photos/500/500?random'},
  {id: 2, name: 'Hoàng Đức', email: 'ducmh02@gmail.com', avatar: 'https://picsum.photos/500/500?random'},
  {id: 3, name: 'Tes01', email: 'test01@gmail.com', avatar: 'https://picsum.photos/500/500?random'},
  {id: 4, name: 'ducmh02', email: 'ducmh02@gmail.com', avatar: 'https://picsum.photos/500/500?random'},
];