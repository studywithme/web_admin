export interface Channel {
    id: number;
    name: string;
    type: string;
    isDelete: boolean;
  }
  
  export interface DATA_MODEL {
    type:string,
    id: number,
    name:string
  }