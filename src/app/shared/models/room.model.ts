export interface Room {
    id: number;
    name: string;
    type: string;
    background: string
    status: string
    maxCount: number;
    limit: number;
  }
  
  export interface DATA_MODEL {
    type:string,
    id: number,
    name:string
  }