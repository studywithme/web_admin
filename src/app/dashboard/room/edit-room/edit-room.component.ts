import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subject, takeUntil } from 'rxjs';
import { RoomService } from 'src/app/services/room-service.service';
import { ROOM_DATA } from 'src/app/shared/constants/room.constant';
import { Channel } from 'src/app/shared/models/channel.model';
import { Room } from 'src/app/shared/models/room.model';

@Component({
  selector: 'app-edit-room',
  templateUrl: './edit-room.component.html',
  styleUrls: ['./edit-room.component.css']
})
export class EditRoomComponent implements OnInit {

  @ViewChild('fileInput') el!: ElementRef;
  roomForm!: FormGroup;
  submitted: boolean = false;
  editFile: boolean = true;
  removeUpload: boolean = false;
  $destroy = new Subject<void>();
  rooms : Room[] = ROOM_DATA
  imageUrl: any = 'https://avataaars.io/?avatarStyle=Transparent&topType=ShortHairShortCurly&accessoriesType=Prescription02&hairColor=Black&facialHairType=Blank&clotheType=Hoodie&clotheColor=White&eyeType=Default&eyebrowType=DefaultNatural&mouthType=Default&skinColor=Light';
  room?: any;
  constructor(
    public fb: FormBuilder, 
    private _router: Router, 
    private cd: ChangeDetectorRef,
    private router: ActivatedRoute,
    private toastr: ToastrService,
    private readonly roomService: RoomService,
  ) { 
    const navigation = this._router.getCurrentNavigation();
    this.room = navigation?.extras.state;
  }


  ngOnInit(): void {
    this.initForm()
  }

  // for easy accessing of form controls
  get f() {
    return this.roomForm.controls;
  }

  initForm() {
    this.roomForm = this.fb.group({
      name: ['',Validators.required],
      type: ['group',[Validators.required,]],
      limit : [5],
      status: []
      // file: ['']
    })
    this.router.params
    .pipe(takeUntil(this.$destroy))
    .subscribe(params => {
      const id = params['id']
      this.roomForm.patchValue({
        id: id,
        name: this.room?.name,
        type: this.room?.type,
        status : this.room?.status,
        limit: this.room?.maxCount
        // file: room?.background
      })
      // this.imageUrl = room?.background
    })
  }

  ngOnDestroy(){
    this.$destroy.next();
    this.$destroy.complete()
  }

  /* Handle form errors in Angular 8 */
  public errorHandling = (control: string, error: string) => {
    return this.roomForm.controls[control].hasError(error);
  }

  update() {
    this.submitted = true;
    if (!this.roomForm.invalid) {
      this.roomService.updateRoom({
        ...this.roomForm.value,
        id: this.room.id
      }).subscribe(res => {
        if (res) {
          this.toastr.success('Edit success!', 'Edit room success!');
          this.cancel();
        } else {
          this.toastr.error('Create fail!', 'Create new room fail!');
        }
      });
    }
  }
  cancel() {
    this._router.navigate(['/', 'rooms']);
  }

  uploadFile(event: any) {
    let reader = new FileReader(); // HTML5 FileReader API
    let file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);

      // When file uploads set it to file formcontrol
      reader.onload = () => {
        this.imageUrl = reader.result;
        this.roomForm.patchValue({
          file: reader.result
        });
        this.editFile = false;
        this.removeUpload = true;
      }
      // ChangeDetectorRef since file is loading outside the zone
      this.cd.markForCheck();
    }
  }

  // Function to remove uploaded file
  removeUploadedFile() {
    let newFileList = Array.from(this.el.nativeElement.files);
    this.imageUrl = 'https://i.pinimg.com/236x/d6/27/d9/d627d9cda385317de4812a4f7bd922e9--man--iron-man.jpg';
    this.editFile = true;
    this.removeUpload = false;
    this.roomForm.patchValue({
      file: [null]
    });
  }

}
