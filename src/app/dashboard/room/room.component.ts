import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RoomService } from 'src/app/services/room-service.service';
import { ROOM_DATA } from 'src/app/shared/constants/room.constant';
import { Room } from 'src/app/shared/models/room.model';
import { User, DATA_MODEL } from 'src/app/shared/models/user.model';
import { ConfirmDialog } from 'src/commons/dialog/confirm.component';
@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'type','status','actions'];
  dataSource = new MatTableDataSource<Room>([]);

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  constructor(
    private _router: Router,
    public dialog: MatDialog, 
    private toastr: ToastrService,
    private readonly roomService: RoomService){}

  ngOnInit(): void {
    this.getAllRoom();
  }

  getAllRoom():void {
    this.roomService.getListRoom().subscribe(res => {
      this.dataSource.data = res.data.filter((e:any) => !e.isDeleted);
    });
  }

  create():void {
    this._router.navigate(['rooms','create']);
  }

  delete(_room:Room): void{
    const data: DATA_MODEL = {
      id: _room.id,
      type: "Room",
      name: _room.name
    }
    const dialog = this.dialog.open(ConfirmDialog, {
      width: '400px',
      data
    })
    dialog.afterClosed().subscribe(res => {
      if (!res) return;
      // Call API delete start
      this.roomService.deleteRoom(_room.id).subscribe(res => {
        if (res && res.result) {
          this.dataSource.data = this.dataSource.data.filter(e => e.id != _room.id);
          this.toastr.success('Delete success!', 'Delete room success!');
        } else {
          this.toastr.error('Delete fail!', 'Delete room fail!');
        }
      });
      // Call API delete end
    })
  }
  edit(room:Room): void{
    this._router.navigate(['rooms','edit',room.id] ,{state: room});
  }

}
