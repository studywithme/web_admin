import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ChannelService} from 'src/app/services/channel-service.service';
import { CHANNEL_DATA } from 'src/app/shared/constants/channel.constant';
import { Channel, DATA_MODEL } from 'src/app/shared/models/channel.model';
import { User } from 'src/app/shared/models/user.model';
import { ConfirmDialog } from 'src/commons/dialog/confirm.component';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.css']
})
export class ChannelComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'type','actions'];
  dataSource = new MatTableDataSource<Channel>([]);

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  constructor(
    private _router: Router,
    public dialog: MatDialog, 
    private readonly channelService: ChannelService,
    private toastr: ToastrService,
    ){}

  ngOnInit(): void {
    this.getAllChannel();
  }

  getAllChannel(): void {
    this.channelService.getListChannel().subscribe(res => {
      this.dataSource.data = res.data.filter((e:any) => !e.isDeleted);
    })
  }

  create():void {
    this._router.navigate(['channels','create']);
  }

  delete(_channel:Channel): void{
    const data: DATA_MODEL = {
      id: _channel.id,
      type: "Chat Channel",
      name: _channel.name
    }
    const dialog = this.dialog.open(ConfirmDialog, {
      width: '400px',
      data
    })
    dialog.afterClosed().subscribe(res => {
      if (!res) return;
      // Call API delete start
      this.channelService.deleteChannel(_channel.id).subscribe(res => {
        if (res && res.data) {
          this.dataSource.data = this.dataSource.data.filter(e => e.id != _channel.id);
          this.toastr.success('Delete success!', 'Delete channel chat success!');
        } else {
          this.toastr.success('Delete fail!', 'Delete channel chat fail!');
        }
      });
      // Call API delete end
    })
  }
  edit(channel:any): void{
    this._router.navigate(['channels','edit',channel.id], {state: channel});
  }

}
