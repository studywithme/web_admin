import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subject, takeUntil } from 'rxjs';
import { ChannelService } from 'src/app/services/channel-service.service';
import { CHANNEL_DATA } from 'src/app/shared/constants/channel.constant';
import { Channel } from 'src/app/shared/models/channel.model';

@Component({
  selector: 'app-edit-channel',
  templateUrl: './edit-channel.component.html',
  styleUrls: ['./edit-channel.component.css']
})
export class EditChannelComponent implements OnInit {

  channelForm!: FormGroup;
  submitted: boolean = false;
  $destroy = new Subject<void>();
  channel?: any;
  constructor(
    public fb: FormBuilder, 
    private _router: Router, 
    private cd: ChangeDetectorRef,
    private router: ActivatedRoute,
    private toastr: ToastrService,
    private readonly channelService: ChannelService
  ) { 
    const navigation = this._router.getCurrentNavigation();
    this.channel = navigation?.extras.state;
  }


  ngOnInit(): void {
    this.initForm()
  }

  // for easy accessing of form controls
  get f() {
    return this.channelForm.controls;
  }

  initForm() {
    this.channelForm = this.fb.group({
      name: ['',Validators.required],
      type: ['',[Validators.required,]],
    })
    this.router.params
    .pipe(takeUntil(this.$destroy))
    .subscribe(params => {
      const id = params['id']
      this.channelForm.patchValue({
        name: this.channel?.name,
        type: this.channel?.type,
      })
    })
  }

  ngOnDestroy(){
    this.$destroy.next();
    this.$destroy.complete()
  }

  /* Handle form errors in Angular 8 */
  public errorHandling = (control: string, error: string) => {
    return this.channelForm.controls[control].hasError(error);
  }

  update() {
    this.submitted = true;
    if (!this.channelForm.invalid) {
      this.channelService.updateChannel({
        id: this.channel.id,
        name : this.channelForm.value.name,
        type: 'static_channel'
      }).subscribe(res => {
        if (res.data) {
          this.toastr.success('Update success!', 'Update channel chat success!');
          this.cancel();
        } else {
          this.toastr.error('Update fail!', 'Update channel chat fail!');
        }
      });
    }
  }
  cancel() {
    this._router.navigate(['/', 'channels']);
  }
}
