import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/user-service.service';
import { ELEMENT_DATA } from 'src/app/shared/constants/user.constant';
import { DATA_MODEL, User } from 'src/app/shared/models/user.model';
import { ConfirmDialog } from 'src/commons/dialog/confirm.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit,AfterViewInit {
  displayedColumns: string[] = ['id','avatar', 'name', 'email','actions'];
  dataSource = new MatTableDataSource<User>([]);

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  constructor(
    private _router: Router,
    public dialog: MatDialog, 
    private toastr: ToastrService,
    private readonly userService: UserService){}

  ngOnInit(): void {
    this.getAll();
  }

  getAll(): void {
    this.userService.getListUser().subscribe(res => {
      this.dataSource.data = res.data.filter((e: any) => !e.block);
      this.dataSource.data = this.dataSource.data.map(e => {
        if (e.name == null) {
          e.name = e.email.split('@')[0];
        }
        return e;
      })
    });
  }

  create():void {
    this._router.navigate(['users','create']);
  }

  delete(_user:User): void{
    const data: DATA_MODEL = {
      id: _user.id,
      type: "User",
      name: _user.name
    }
    const dialog = this.dialog.open(ConfirmDialog, {
      width: '400px',
      data
    })
    dialog.afterClosed().subscribe(res => {
      if (!res) return;
      // Call API delete start
      this.userService.blockUser({id: _user.id}).subscribe(res => {
        if (res && res.data) {
          this.dataSource.data = this.dataSource.data.filter(e => e.id != _user.id);
          this.toastr.success('Block success!', 'Block user success!');
        } else {
          this.toastr.success('Block success!', 'Block user success!');
        }
      });
      // Call API delete end
    })
  }
  edit(user:User): void{
    this._router.navigate(['users','edit',user.id]);
  }



}
