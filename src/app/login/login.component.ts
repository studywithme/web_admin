import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string = '';
  password: string = '';
  constructor(private _router: Router, private readonly authService: AuthService) { }
  
  ngOnInit() {
  }

  login(){
    // this.authService.login({username: this.username, password: this.password}).subscribe(res => {
    //   console.log("Res: ", res);
      
    //   if (res && res.token) {
    //     localStorage.setItem("token",res.token);
    //     this._router.navigate(['/']);
    //   } 
    // })

    this.authService.getData().subscribe(res => {
      localStorage.setItem("token",res.token)
      localStorage.setItem("user",JSON.stringify(res.user))
      this._router.navigate(['/']);
      // console.log(res);
    })
    
  }

}
