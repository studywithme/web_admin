import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChannelService {

  constructor(private http: HttpClient){
  }
  
  baseURL = 'https://studywithmeserver12.herokuapp.com';
  url: string = `${this.baseURL}/api/chat`;


  getListChannel(): Observable<any>{
    return this.http.get(`${this.url}/allChannel/static_channel`);
  }

  createChannel(data: any): Observable<any>{
    return this.http.post(`${this.url}/channel/create`, data);
  }

  updateChannel(data: any): Observable<any>{
    return this.http.put(`${this.url}/channel/update`, data);
  }

  deleteChannel(id: any): Observable<any>{
    return this.http.delete(`${this.url}/channel/${id}`);
  }
}
