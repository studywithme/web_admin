import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LOGIN } from '../shared/models/login.model';
import { BaseApiService } from './base-api.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  requestOptions = {}
  constructor(private http: HttpClient){
  }

  baseURL = 'https://studywithmeserver12.herokuapp.com'
  url: string = `${this.baseURL}/api/auth/login`
  getData(): Observable<LOGIN>{
    return this.http.post<LOGIN>(this.url,
      {email: 'admin1@gmail.com', 
      password : "123456"
      })
    // admin1@gmail.com, 123456
  }


  getList(): Observable<any>{
    return this.http.get(`${this.baseURL}/api/user/all`)
  }
}
