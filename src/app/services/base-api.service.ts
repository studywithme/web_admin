import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { env } from 'process';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BaseApiService {

  private baseUrl  = 'http://localhost:3200/api/';

  constructor(
    private readonly http: HttpClient
  ) { }

  getHeaders() {
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
    })
    return headers;
  }

  get(url: string, params: any) {
    return this.http.get(this.baseUrl + url,{ headers: this.getHeaders(), params })
  }

  post(url: string, data: any) {
    return this.http.post(this.baseUrl + url, data, { headers: this.getHeaders(), })
  }

  put(url: string, data: any) {
    return this.http.put(this.baseUrl + url, data, { headers: this.getHeaders(), })
  }

  delete(url: string, params: any) {
    return this.http.delete(this.baseUrl + url,{ headers: this.getHeaders(), params})
  }
  
}
