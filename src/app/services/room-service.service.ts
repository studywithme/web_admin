import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoomService{

  constructor(private http: HttpClient){
  }
  
  baseURL = 'https://studywithmeserver12.herokuapp.com';
  url: string = `${this.baseURL}/api/room`;


  getListRoom(): Observable<any>{
    return this.http.get(`${this.url}/list`);
  }

  createRoom(data: any): Observable<any>{
    return this.http.post(`${this.url}/create`, data);
  }

  updateRoom(data: any): Observable<any>{
    return this.http.post(`${this.url}/update`, data);
  }

  deleteRoom(id: any): Observable<any>{
    return this.http.delete(`${this.url}/${id}`);
  }
}
