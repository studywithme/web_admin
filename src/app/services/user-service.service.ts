import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient){
  }
  
  baseURL = 'https://studywithmeserver12.herokuapp.com'
  url: string = `${this.baseURL}/api/user`


  getListUser(): Observable<any>{
    return this.http.get(`${this.url}/all`)
  }

  blockUser(data: any): Observable<any>{
    return this.http.post(`${this.url}/block`, data)
  }
}
